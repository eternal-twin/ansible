#!/usr/bin/env oil
var PROJECT_ROOT = $(cd "$_this_dir" { pwd })

cd $PROJECT_ROOT {
  ansible-playbook playbooks/users/main.yml --ask-pass --ask-vault-pass  --extra-vars "ansible_user=arch"
}
